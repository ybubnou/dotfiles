import System.Exit
import System.IO (Handle, hPutStrLn)

import qualified Data.Map as M
import Graphics.X11.Xlib

import XMonad

import qualified XMonad.StackSet as W

import XMonad.Prompt.Shell
import XMonad.Prompt

-- utils
import XMonad.Util.Run (spawnPipe)
import XMonad.Hooks.SetWMName

-- hooks
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog


-- layouts
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Layout.ResizableTile

import XMonad.Actions.CycleWS

-- Main --
my_terminal = "urxvt256c-ml"

myStartupHook = startup
startup :: X ()
startup = do
    setWMName "LG3D"
    spawn "setxkbmap -option lv3:ralt_alt,grp:alt_shift_toggle,grp:switch,grp_led:scroll -layout 'us,ru'"


main = do
       h <- spawnPipe "xmobar"
       xmonad $ docks defaultConfig
              { workspaces = workspaces'
              , modMask = modMask'
              , borderWidth = borderWidth'
              , normalBorderColor = normalBorderColor'
              , focusedBorderColor = focusedBorderColor'
              , terminal = my_terminal
              , keys = keys'
              , logHook = logHook' h
              , layoutHook = layoutHook'
              , manageHook = myManageHook
              , startupHook = myStartupHook
              }


myManageHook = composeAll
   [ className =? "Google"      --> doShift "1:google"
   , className =? "Firefox"     --> doShift "7:firefox"
   , className =? "Skype"       --> doShift "8:skype"
   , className =? "stalonetray" --> doIgnore
   , manageDocks <+> manageHook defaultConfig
   ]


logHook' :: Handle ->  X ()
logHook' h = dynamicLogWithPP $ customPP { ppOutput = hPutStrLn h }

-- Looks --
-- bar
customPP :: PP
customPP = defaultPP { ppCurrent = xmobarColor "#ffffff" ""
                     , ppTitle =  shorten 80
                     , ppSep =  "<fc=#bdbdbd>  |  </fc>"
                     , ppHiddenNoWindows = xmobarColor "#AFAF87" ""
                     , ppUrgent = xmobarColor "#FFFFAF" "" . wrap "[" "]"
                     }

-- borders
borderWidth' :: Dimension
borderWidth' = 3

normalBorderColor', focusedBorderColor' :: String
normalBorderColor'  = "#333333"
focusedBorderColor' = "#388E3C"

-- workspaces
workspaces' :: [WorkspaceId]
workspaces' = ["1  :: ", "2  :: ", "3  :: ", "4  :: ", "5  :: ", "6  :: ", "7  :: ", "8  :: ", "9  :: ", "0  "]

-- layouts
myLayout = avoidStruts $ smartBorders tiled ||| smartBorders (Mirror tiled) ||| noBorders Full
  where
    tiled = ResizableTall 1 (2/100) (1/2) []

myGridLayout = avoidStruts $ noBorders Full |||  Grid ||| Full ||| GridRatio (4/3)

-- layout `myIMLayout` will be used on workspace "8"
-- layout `myGridLayout` will be used on all others desktops
layoutHook' = myLayout

-- Keys/Button bindings --
-- modMaskask
modMask' :: KeyMask
modMask' = mod4Mask


-- keys
keys' :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
keys' conf@(XConfig {XMonad.modMask = modMask}) = M.fromList $
    -- launching and killing programs
    [ ((modMask, xK_x          ), spawn my_terminal)
    , ((modMask, xK_r          ), shellPrompt defaultXPConfig)

    -- Kill focused window (Win+Backspace).
    , ((modMask, xK_BackSpace),
        kill)

    -- layouts
    , ((modMask,               xK_space ), sendMessage NextLayout)
    , ((modMask .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modMask,               xK_b     ), sendMessage ToggleStruts)


    -- refresh
    , ((modMask,               xK_n     ), refresh)

    -- focus
    , ((modMask,               xK_Tab   ), windows W.focusDown)
    , ((modMask,               xK_j     ), windows W.focusDown)
    , ((modMask,               xK_k     ), windows W.focusUp)

    -- swapping
    , ((modMask .|. shiftMask, xK_Return), windows W.swapMaster)
    , ((modMask .|. shiftMask, xK_j     ), windows W.swapDown  )
    , ((modMask .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- increase or decrease number of windows in the master area
    , ((modMask, xK_comma),
        sendMessage (IncMasterN 1))

    , ((modMask, xK_period),
        sendMessage (IncMasterN (-1)))

    -- resizing
    {-, ((modMask,               xK_h     ), sendMessage Shrink)-}
    {-, ((modMask,               xK_l     ), sendMessage Expand)-}
    , ((modMask .|. shiftMask, xK_h     ), sendMessage MirrorShrink)
    , ((modMask .|. shiftMask, xK_l     ), sendMessage MirrorExpand)

    , ((modMask .|. shiftMask, xK_Left),
        sendMessage Shrink)

    , ((modMask .|. shiftMask, xK_Right),
        sendMessage Expand)

    , ((mod1Mask, xK_q), nextWS)
    , ((mod1Mask, xK_s), prevWS)

    {-, ((modMask .|. shiftMask, xK_Left),-}
        {-sendMessage Shrink)-}

    {-, ((modMask .|. shiftMask, xK_Right),-}
        {-sendMessage Expand)-}

    {-, ((modMask .|. shiftMask, xK_Down),-}
        {-sendMessage (IncreaseDown 1))-}

    {-, ((modMask .|. shiftMask, xK_Up),-}
        {-sendMessage (IncreaseUp 1))-}

    {-, ((modMask .|. controlMask, xK_Left),-}
        {-sendMessage (DecreaseLeft 1))-}

    {-, ((modMask .|. controlMask, xK_Right),-}
        {-sendMessage (DecreaseRight 1))-}

    {-, ((modMask .|. controlMask, xK_Down),-}
        {-sendMessage (DecreaseDown 1))-}

    {-, ((modMask .|. controlMask, xK_Up),-}
        {-sendMessage (DecreaseUp 1))-}

    -- Lock the screen using slock (Win+l).
    , ((modMask, xK_l),
        spawn "slock")

    , ((modMask, xK_Print),
        spawn "scrot screen_%Y-%m-%d-%H-%M-%S.png -d 1")

    -- mpd controls
    {-, ((modMask .|. controlMask,  xK_h     ), spawn "mpc prev")-}
    {-, ((modMask .|. controlMask,  xK_n     ), spawn "mpc play")-}
    {-, ((modMask .|. controlMask,  xK_s     ), spawn "mpc next")-}
    {-, ((modMask .|. controlMask,  xK_g     ), spawn "mpc seek -2%")-}
    {-, ((modMask .|. controlMask,  xK_c     ), spawn "mpc volume -4")-}
    {-, ((modMask .|. controlMask,  xK_r     ), spawn "mpc volume +4")-}
    {-, ((modMask .|. controlMask,  xK_l     ), spawn "mpc seek +2%")-}
    -- quit, or restart
    , ((modMask .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
    , ((modMask              , xK_q     ), restart "xmonad" True)
    ]
    ++
    -- mod-[1..9] %! Switch to workspace N
    [((mod1Mask, k), windows (W.view i))
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]]
    ++
    -- mod-win-[1..9] %! Move client to workspace N
    [((modMask, k), windows (W.shift i))
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]]
-------------------------------------------------------------------------------
