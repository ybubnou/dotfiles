set nocompatible

" Load required plugins.
call plug#begin('~/.vim/plugged')
Plug 'altercation/vim-colors-solarized'
Plug 'fatih/vim-go'
Plug 'Shougo/unite.vim'
Plug 'Shougo/neocomplete.vim'
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive' " Show branch in airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()


" Set a default vim configuration.
syntax on
filetype on
filetype plugin on
filetype plugin indent on
colorscheme solarized

set laststatus=2 " Always display vim-airline.
set backspace=indent,eol,start
set guifont=PowerlineSymbols
set encoding=utf-8
set t_Co=256
set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set hlsearch
set incsearch " Search matches as you type.
set nu

let g:go_autodetech_gopath = 1
let g:go_gocode_autobuild = 0
let g:go_fmt_command = "gofmt"
let g:go_bin_path = expand("$GOPATH/bin")

" Use italic font for comments.
highlight Comment cterm=italic


" Use the same theme for AirLine as a color scheme.
let g:airline_theme='solarized'
let g:airline_powerline_fonts = 1


" Neocomplete configuration
let g:neocomplete#enable_auto_close_preview = 1
let g:neocomplete#enable_at_startup = 1
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length=1


autocmd FileType python setlocal omnifunc=pythoncomplete#Complete

" Start manual completion with ctrl+space
inoremap <expr> <C-Space> neocomplete#start_manual_complete()
let g:neocomplete#sources#omni#input_patterns = {}
